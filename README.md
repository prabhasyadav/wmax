## _W<sub>max</sub>_
---


The codes provide a method to compute _W<sub>max</sub>_ . This is required for the assessment of contaminated sites.

The code is Python based. 

Check at [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/prabhasyadav%2Fwmax/master)

CC-by-4.0

